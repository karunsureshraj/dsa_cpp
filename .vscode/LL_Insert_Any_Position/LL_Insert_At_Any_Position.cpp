#include "stdio.h"
#include "iostream"
using namespace std;

struct Node
{
    int data;
    Node* next;
};

Node* head;

void Insert (int val, int pos){
    Node* temp = new Node();
    temp->data = val;
    
    if(pos ==1){
        temp->next = head;
        head = temp;
        return;
    }

    Node* tempHead = head;
    for(int i=0; i<pos-2; i++){
        tempHead = tempHead->next;
    }

    temp->next = tempHead->next;
    tempHead->next = temp;
}

void Print(){
    Node* temp = head;

    cout<< "The list is: ";
    while(temp != NULL){
        cout << temp->data << " ";
        temp = temp->next;
    }
    cout << "\n";
}

int main()
{
    head = NULL;
    /*
        p - position
        v - value
        s - seperator
    */

    /*
    int count,v,s,p;
    cout << "Enter the number of values\n";
    cin >> count;

    for (int i = 0; i < count; i++)
    {
        cout << "Enter the value and position :";
        cin >> v >> s >> p ;
        Insert(v,p);
        Print ();
    }
    */

    Insert(19,1);
    Insert(10,2);
    Insert(100,3);
    Insert(40,4);
    Print();
    Insert(1000,2);
    Print();
    return 0;
}