#include "stdio.h"
#include "iostream"
#include "stack"
using namespace std;

stack<char> S;
void Reverse(char *C, int n){
    for (int i = 0; i < n; i++)
    {
        S.push(C[i]);
    }

    for (int i = 0; i < n; i++)
    {
        C[i] = S.top();
        S.pop();
    }
}

int main(){
    char C[100];
    cout << "Enter a String: ";
    gets(C);
    Reverse(C,strlen(C));
    cout << "Reversed String is: " << C << "\n";
    return 0;
}