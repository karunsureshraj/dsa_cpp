#include "stdio.h"
#include "iostream"
#include "stack"
using namespace std;

stack<char> S;

bool IsMatchingPair(char opening, char closing){
    if(opening == '(' && closing == ')') 
        return true;
    else if (opening == '[' && closing == ']'){
        return true;
    }
    else if (opening == '{' && closing == '}'){
        return true;
    }
    return false;    
}

bool Reverse(string e){
    char symbol;
    for (int i = 0; i < e.length(); i++)
    {
        if(e[i] == '{' || e[i] == '(' || e[i] == '['){
            cout << "Match found, pushing " << "'" << e[i] << "'" << " to Stack.\n";
            S.push(e[i]);
        }
        else if(e[i] == '}' || e[i] == ')' || e[i] == ']'){
            symbol = S.top();
            cout << "Comparing " << e[i] << " with Stack top value: " << symbol <<".\n";
            if(S.empty() == true || !IsMatchingPair(symbol, e[i])){
                cout << "No matching symbol found. Exiting.\n";
                return false;
            }
            S.pop();
        }
    }  
    return S.empty();
}

int main(){
    string expression;

    cout << "Enter an Expression: \n";
    cin >> expression;
    cout << "The expression is: " << expression << "\n";

    if(Reverse(expression)){
        cout << "Expression is valid.\n";
    }
    else{
        cout << "Expression is invalid.\n";
    }
    return 1;
}