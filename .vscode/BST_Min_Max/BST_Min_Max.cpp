#include "stdio.h"
#include "iostream"
using namespace std;

struct BSTNode
{
    int data;
    BSTNode *left;
    BSTNode *right;
};
BSTNode *root;
int min =0;

int FindMin(BSTNode *root){
    if(root == NULL){
        return -1;
    }else if(root->left == NULL){
        return root->data;
    }
    return FindMin(root->left);
}

BSTNode * GenerateNewNode(BSTNode *root, int data){
    BSTNode *node = new BSTNode();
    node->data = data;
    node->left = NULL;
    node->right = NULL;
    return node;
}
/* 
    To Add a Lesser Value to a Tree
    Idea is to traverse towards Left most until we find the LEAF Node
    New node gets added only to a leaf Node
    Once we find such a LEAF Node:
        Create new Node, Assign Data to that Node
        Assign the address of new Node as left Child of current LEAF Node
    
 */

BSTNode * Insert(BSTNode *root, int data){
    if(root == NULL){
        BSTNode *node = GenerateNewNode(root, data);
        root = node;
    }else if(data <= root->data){
        root->left = Insert(root->left, data);
    }else if(data > root->data){
        root->right = Insert(root->right, data);
    }
    return root;
}

int main(){
    root = NULL;
    root = Insert(root, 50);
    root = Insert(root, 76);
    root = Insert(root, 21);
    root = Insert(root, 4);
    root = Insert(root, 32);
    root = Insert(root, 64);
    cout << "The min is: " << FindMin(root) << "\n";
    return 0;
}