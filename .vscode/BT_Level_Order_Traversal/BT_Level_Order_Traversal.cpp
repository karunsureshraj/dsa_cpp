#include "stdio.h"
#include "iostream"
#include "queue"
using namespace std;

struct Node
{
    char data;
    Node * left;
    Node * right;
};
Node * root;
queue<Node *> Q;

Node * GenerateNode(char d){
    Node *temp = new Node();
    temp->data = d;
    temp->right = NULL;
    temp->left = NULL;
    return temp;
}

void Insert(char d){
    Node *temp = root;
    if(temp == NULL){
        cout << "Inserting " << d << " at the Root.\n";
        root = GenerateNode(d);
        return;
    }
    while(temp!=NULL){
        if(d <= temp->data){
            if(temp->left==NULL){
                cout << "Value is Smaller, Inserting " << d << " to the left of Node with value: " 
                    << temp->data << ".\n";
                temp->left = GenerateNode(d);
                return;
            }else{
                temp = temp->left;
            }
        }else{
            if(temp->right==NULL){
                cout << "Value is Larger, Inserting " << d << " to the right of Node with value: " 
                << temp->data << ".\n";
                temp->right = GenerateNode(d);
                return;
            }else{
                temp = temp->right;
            }
        }
    }
    GenerateNode(d);
}

void LevelOrder (){
    if(root == NULL){
        return;
    }
    cout << "Result of Level Order Traversal is: ";
    Q.push(root);
    while(!Q.empty()){
        Node *temp = Q.front();
        cout << temp->data << " ";
        if(temp->left!=NULL)
            Q.push(temp->left);
        if(temp->right!=NULL)
            Q.push(temp->right);
        Q.pop();
    }
    cout << "\n";
}

int main(){
    root = NULL;
    Insert('F');
    Insert('D');
    Insert('J');
    Insert('B');
    Insert('E');
    Insert('G');
    Insert('K');
    Insert('A');
    Insert('C');
    Insert('I');
    Insert('H');

    LevelOrder();
    return 0;
}