#include "stdio.h"
#include "iostream"
using namespace std;


int Q[10];
int front = -1;
int rear = -1;
int N = sizeof(Q);

bool IsFull(){
    if((rear+1)%N ==front)
        return true;
    return false;
}

bool IsEmpty(){
    if(front == -1 and rear == -1)
        return true;
    return false;
}

void Enqueue(int x){
    if(IsFull()){
        cout << "Queue is Full.\n";
        return;
    }else if(IsEmpty()){
        front = 0;
        rear = 0;
    }else{
        rear = (rear+1) % N;
    }
    Q[rear] = x;
}

void Dequeue(){
    if(IsEmpty()){
        cout << "Queue is empty. Nothing to Dequeue.\n";
        return;
    }else if(front==rear){
        front = -1;
        rear = -1;
    }else{
        front = (front + 1) % N;
    }
    cout << "front is: " << front << " & Rear is: " << rear << "\n";
}

void Print(){
    if(IsEmpty()){
        return;
    }
    cout << "Queue is: ";
    for (int i = front; i <= rear; i++)
    {
        cout << Q[i] << " ";
    }
    cout << "\n";   
}

int main(){
    Enqueue(10);
    Enqueue(20);
    Enqueue(30);
    Print();
    cout << "Performing a Dequeue Operation.\n";
    Dequeue();
    Print();
    cout << "Performing a Dequeue Operation.\n";
    Dequeue();
    Print();
    cout << "Performing a Dequeue Operation.\n";
    Dequeue();
    Print();

    Enqueue(10);
    Print();
    return 0;
}