#include "stdio.h"
#include "iostream"
using namespace std;

struct BTNode
{
    int data;
    BTNode *right;
    BTNode *left;
};
BTNode *root;

BTNode * GenerateNode(int data){
    BTNode *node = new BTNode();
    node->data = data;
    node->left = NULL;
    node->right = NULL;
    return node;
}

BTNode * Insert(BTNode *root, int data){
    if(root == NULL){
        root = GenerateNode(data);
    }else if (data <= root->data){
        root->left = Insert(root->left, data);
    }else{
        root-> right = Insert(root->right, data);
    }
    return root;
}

int FindHeight (BTNode *node){
    if(node == NULL){
        return -1;
    }
    return (max(FindHeight(node->left), FindHeight(node->right)) + 1);
}

int main(){
    root = NULL;
    root = Insert(root,15);
    root = Insert(root,10);
    root = Insert(root,20);
    root = Insert(root,8);
    root = Insert(root,12);
    root = Insert(root,17);
    root = Insert(root,25);
    root = Insert(root,3);
    cout << "Height of the Tree is: " << FindHeight(root) << "\n";
    return 0;
}