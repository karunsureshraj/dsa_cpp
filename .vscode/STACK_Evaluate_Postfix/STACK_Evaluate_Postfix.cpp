#include "stdio.h"
#include "iostream"
#include "stack"
using namespace std;

stack<int> S;
string expression = "2 3 * 5 4 * + 9 -";

bool IsOperand(char s){
    if (s >= '0' && s <= '9'){
        return true;
    }
    return false;
}

bool IsOperator(char s){
    if (s == '+' || s == '-' || s == '*' || s == '/'){
        return true;
    }
    return false;
}

int PerformOperation(char operand, int Op1, int Op2){
    if(operand == '+'){
        return Op1 + Op2;
    }else if(operand == '-'){
        return Op1 - Op2;
    }else if(operand == '*'){
        return Op1 * Op2;
    }else if(operand == '/'){
        return Op1 / Op2;
    }
    return 0;
}

int main(){
    for (int i = 0; i < expression.length(); i++)
    {
        if(IsOperand(expression[i])){
            cout << "Found an Operand: " << expression[i] << ". Pushing to Stack." << " \n";
            int operand = (int)expression[i] - 48;
            S.push(operand);
        }
        else if(IsOperator(expression[i])){
            int Op2 = S.top();
            S.pop();
            int Op1 = S.top();
            S.pop();
            int result = PerformOperation(expression[i], Op1, Op2);
            cout << "Operator: " << expression[i] << " Operands are: " << Op1 << " & " << Op2 << " Result: " << result << "\n";
            S.push(result);
        }
    }
    cout << "The result is: " << S.top() << "\n";
    return 1;
}