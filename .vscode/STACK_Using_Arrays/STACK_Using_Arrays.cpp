#include "stdio.h"
#include "iostream"
#define MAX_SIZE 101
using namespace std;

int A[MAX_SIZE];
int top = -1;

bool IsEmpty(){
    if (top == -1)
    {
        return true;
    }
    return false;
}
bool IsFull(){
    if(top == MAX_SIZE-1){
        return true;
    }
    return false;
}
void Push(int x){
    if(IsFull() ){
        cout << "Stack is full. Please Pop out some items before trying to push new items.\n";
        return;
    }
    A[++top] = x;
}

int Pop(){
    if(IsEmpty()){
        cout << "Stack is Empty. There are no items to pop.\n";
        return 0;
    }
    return A[top--];
}

void Print(){
    cout << "Stack is: ";
    for (int i = 0; i <= top; i++)
    {
        cout << A[i] << " ";
    }
    cout<< "\n";    
}

int main(){
    Pop();
    Push(10);
    Push(200);
    Push(150);
    Push(20);
    Print();
    cout<<"Popping out..\n";
    Pop();
    Print();

    return 0;
}