#include "stdio.h"
#include "iostream"
using namespace std;

struct Node
{
    int data;
    Node *next;
};

Node *head, *tail;

void Enqueue(int val){
    Node *temp = new Node();
    temp->data = val;
    temp->next = NULL;

    Node *temp1 = tail;
    if(temp1 == NULL){
        tail = temp;
        head = temp;
        return;
    }
    temp1->next = temp;
    tail = temp;
}

void Dequeue(){
    Node *temp = head;
    if (head == NULL){
        cout << "Empty Queue.\n";
    }else if(head == tail){
        head = tail = NULL;
    }else{
        head = head->next;  
    }
    /* 
        You cant just write free(head) as head now points to the new Queue Head Node.
        So first have a temp variable store the current Head Node and then use that temp pionter
        to delete the Node
     */
    free(temp);
}

void Print(){
    cout << "Performing a Dequeue.\n";
    Node *temp = head;
    cout << "Queue is: ";
    while(temp!=NULL){
        cout << temp->data << " ";
        temp = temp->next;
    }
    cout << "\n";
}

int main(){
    head = NULL;
    tail = NULL;
    Enqueue(10);
    Enqueue(90);
    Enqueue(30);
    Enqueue(110);
    Enqueue(50);
    Print();
    Dequeue();
    Print();
    Dequeue();
    Print();
    return 0;
}