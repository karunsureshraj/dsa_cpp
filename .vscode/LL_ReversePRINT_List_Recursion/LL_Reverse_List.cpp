#include "stdio.h"
#include "iostream"
using namespace std;

struct Node
{
    int data;
    Node* next;
};

Node* head;
void Insert(int p, int v){
    Node* temp = new Node();
    temp->data = v;

    if(p==1){
        temp->next = head;
        head = temp;
        return;
    }

    Node* temp1 = head;
    for (int i = 0; i < p-2; i++)
    {  
        temp1 = temp1->next;
    }
    
    temp->next = temp1->next;
    temp1->next = temp;

}

void Print(){
    Node* temp = head;
    cout << "The list is: \n";
    while(temp!=NULL){
        cout << temp->data << " ";
        temp = temp->next;
    }
    cout << "\n";
}

void Reverse(){
/*  Node *temp, *prev, *next;
    temp = head;
    prev = NULL;

    while(temp!=NULL){
        next = temp->next;
        temp->next = prev;
        prev = temp;
        temp = next;
    } */
    
    Node *current, *prev, *next;
    current = head;
    prev = NULL;

    while(current!=NULL){
        next = current->next;
        current->next = prev;
        prev = current;
        current = next;
    }
    head = prev;
}

int main(){
    head = NULL;
    Insert(1,100);
    Insert(2,399);
    Insert(3,250);
    Insert(4,222);
    Print();
    cout << "Inserting value at position 2\n";
    Insert(2,999);
    Print();
    cout << "Reversing the List\n";
    Reverse();
    Print();
    return 0;
}