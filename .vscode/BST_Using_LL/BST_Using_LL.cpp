#include "stdio.h"
#include "iostream"
using namespace std;

struct BSTNode
{
    int data;
    BSTNode *left;
    BSTNode *right;
};

BSTNode *root;

BSTNode * CreateNode (int data){
    BSTNode *temp = new BSTNode();
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}
void Insert(int data){
    BSTNode *temp = CreateNode(data);
    BSTNode *tempRoot = root;

    if(root ==NULL){
        cout << "Added as Root Node.\n";
        root = temp;
        return;
    }
    while (true){
        if(data <= tempRoot->data){
            cout << "Comparing '" << data << "' with Node having data: " << tempRoot->data << " \n";
            if(tempRoot->left == NULL){
                cout << "Adding the value to Left.\n";
                tempRoot->left = temp;
                return;
            }else{
                tempRoot = tempRoot->left;
            }
        }else if(data>tempRoot->data){
            cout << "Comparing '" << data << "' with Node having data: " << tempRoot->data << " \n";
            if(tempRoot->right == NULL){
                cout << "Adding the value to Right.\n";
                tempRoot->right = temp;
                return;
            }else{
                tempRoot = tempRoot->right;
            }
        }
    }
}

bool Search(BSTNode *root, int data){
    if(root == NULL){
        return false;
    }else if(root->data==data){
        return true;
    }else if(data <= root->data){
        return Search(root->left, data);
    }else {
        return Search(root->right, data);
    }
}

int main(){
    root = NULL;
    Insert(50);
    Insert(76);
    Insert(21);
    Insert(4);
    Insert(32);
    Insert(64);
    if(Search(root,31))
        cout << "Value found.\n";
    else
        cout << "Value not found.\n";
    return 0;
}