#include "stdio.h"
#include "iostream"
using namespace std;

struct Node{
    int data;
    Node *next;
};

Node* head;

void Insert(int p, int v){
    Node *temp = new Node();
    temp->data = v;

    if(p==1){
        temp->next = head;
        head = temp;
        return;
    }

    Node *temp1 = head;
    for (int i = 0; i < p-2; i++)
    {
        temp1 = temp1->next;
    }
    
    temp->next = temp1->next;
    temp1->next = temp;
}

void Print(){
    Node *temp = head;
    cout << "Printing the list: ";
    while(temp!=NULL){
        cout << temp->data << " ";
        temp = temp->next;
    }
    cout << "\n";
}

void ReverseList(Node *current){
    if(current->next==NULL){
        head = current;
        return;
    }
    ReverseList(current->next);
    Node *prev = current->next;
    prev->next = current;
    current->next = NULL;
}

int main(){
    head = NULL;
    Insert(1,35);
    Insert(2,99);
    Insert(3,45);
    Insert(4,2);
    Insert(5,5);
    Insert(6,100);
    Print();
    cout << "Inserting element at position 1\n";
    Insert(1,66);
    Print();
    cout << "Reversing the list using Recursion\n";
    ReverseList(head);
    Print();

    return 0;
}