#include "stdio.h"
#include "iostream"
using namespace std;

struct BSTNode
{
    int data;
    BSTNode *left;
    BSTNode *right;
};
BSTNode *head;

BSTNode * CreateNode(int data){
    BSTNode *temp = new BSTNode();
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}

BSTNode * Insert(BSTNode *root, int data){
    if(root == NULL){
        root = CreateNode(data);
    }else if(data <= root->data){
        root->left = Insert(root->left, data);
    }else if(data > root->data){
        root->right = Insert(root->right, data);
    }
    return root;
}

int main(){
    head = NULL;

    return 0;
}