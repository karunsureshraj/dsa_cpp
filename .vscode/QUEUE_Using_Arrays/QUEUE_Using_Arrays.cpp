#include "stdio.h"
#include "iostream"
using namespace std;

int Q[10];
int front = -1;
int rear = -1;

/* 
    In this implementation, 'rear' will only advance in forward direction
    Hence once rear == Size of Queue, no more elements can be added
 */
bool IsFull(){
    if(rear == sizeof(Q)-1)
        return true;
    return false;
}

bool IsEmpty(){
    if(rear == -1 && front == -1)
        return true;
    return false;
}

void Enqueue(int x){
    if(IsFull()){
        cout << "Queue is Full. Delete some values before Enqueueng.\n";
        return;
    }else if(IsEmpty()){
        front = 0;
        rear = 0;
    }else{
        rear+= 1;
    }
    Q[rear] = x;
}

void Dequeue(){
    if(IsEmpty()){
        cout << "Queue is empty. Nothing to Dequeue.\n";
    }else if(front == rear){
        front = -1;
        rear = -1;
    }else{
        front +=1;
    }
}

void Print(){
    if(IsEmpty()){
        cout << "The queue is Empty.\n";
        return;
    }
    cout << "The Queue is: ";
    for(int i=front; i<=rear; i++){
        cout << Q[i] << " ";
    }
    cout << "\n";
}

int main(){
    Enqueue(10);
    Enqueue(20);
    Enqueue(30);
    Print();
    cout << "Performing a Dequeue Operation.\n";
    Dequeue();
    Print();
    cout << "Performing a Dequeue Operation.\n";
    Dequeue();
    Print();
    cout << "Performing a Dequeue Operation.\n";
    Dequeue();
    Print();

    Enqueue(10);
    Print();
    return 0;
}