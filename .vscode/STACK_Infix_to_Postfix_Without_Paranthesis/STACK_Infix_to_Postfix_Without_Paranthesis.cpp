#include "stdio.h"
#include "iostream"
#include "stack"
using namespace std;

//string Infix = "A+B*C-D*E";
string Infix = "A^B^C/D*E";
string Postfix;
stack<char> OperatorStack;

bool IsOperand(char token){
    if(token >='0' && token<= '9'){
        return true;
    } else if (token >='A' && token<= 'Z'){
        return true;
    }
    return false;
}

bool IsOperator(char token){
    if(token == '+' || token == '-' || token == '/' || token == '*' || token == '^')
        return true;
    return false;
}

int IdentifyWeight(char c){
    int weight = -1;
    switch (c)
    {
    case '+':
    case '-':
        weight = 1;
        break;
    case '*':
    case '/':
        weight = 2;
        break;
    case '^':
        weight = 3;
        break;
    }
    return weight;
}
/* 
    When it comes to Operators with equal precedence, except for exponents, it follows Left to Right
    rule (Left Associative Rule). If incase you found a token having equal preference as top of stack,
    then since we are evaluating from left to right, you already have the token within the stack. 
    So you just need to pop the stack.
 */
bool IsTopOfStackOfHigherOrder(char top, char token){
    int w1 = IdentifyWeight(top);
    int w2 = IdentifyWeight(token);

    if(w1==w2){
        if(token == '^'){
            return false;
        }else{
            return true;
        }
    }
    return w1>w2 ? true: false;
}

void InfixtoPostfix(string Infix){
    for (int i = 0; i < Infix.length(); i++)
    {
        if(IsOperand(Infix[i])){
            Postfix+= Infix[i];
        }else if(IsOperator(Infix[i])){
            while(!OperatorStack.empty() && IsTopOfStackOfHigherOrder(OperatorStack.top(), Infix[i])){
                Postfix+= OperatorStack.top();
                OperatorStack.pop();
            }
            OperatorStack.push(Infix[i]);
        }
    }
    while(!OperatorStack.empty()){
        Postfix+= OperatorStack.top();
        OperatorStack.pop();
    }
    cout << "The Postfix Expression is: " << Postfix << "\n";
}

int main(){
    InfixtoPostfix(Infix);
    return 0;
}