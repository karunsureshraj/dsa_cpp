#include "stdio.h"
#include "iostream"
#include "stack"
using namespace std;

stack<struct Node*> S;
struct Node
{
    int data;
    Node *next;
};
Node *head;


void Insert(int p, int v){
    Node *temp = new Node();
    temp->data = v;

    if(p==1){
        temp->next = NULL;
        head = temp;
        return;
    }
    Node *temp1 = head;
    for (int i = 0; i < p-2; i++)
    {
        temp1 = temp1->next;
    }

    temp->next = temp1->next;
    temp1->next = temp;
}

void Print(){
    Node *temp = head;
    cout << "The list is: ";
    while(temp!=NULL){
        cout << temp->data << " ";
        temp = temp->next;
    }
    cout << "\n";
}

void StoretoStack(){
    Node *temp = head;
    while(temp!=NULL){
        S.push(temp);
        temp = temp->next;
    }
}

void PrintStack(){
    cout << "Contents of Stack is: ";
    while(!S.empty()){
        cout << S.top() << " ";
        S.pop();
    }
    cout << "\n";
}

void Reverse(){
    StoretoStack();
    Node *temp = S.top();
    head = temp;
    S.pop();
    while(!S.empty()){
        temp->next = S.top();
        S.pop();
        temp = temp->next;
    }
    temp->next = NULL;
}

int main(){
    head = NULL;
    Insert(1,200);
    Insert(2,99);
    Insert(3,666);
    Print();
    cout<<"Inserting at position 2\n";
    Insert(2,444);
    Print();
    StoretoStack();
    PrintStack();
    cout << "Reversing the list.\n";
    Reverse();
    Print();

    return 0;
}