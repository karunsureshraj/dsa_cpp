#include "stdio.h"
#include "iostream"
using namespace std;

struct Node
{
    int data;
    Node *prev;
    Node *next;
};

Node *head;

void InsertAtHead(int x){
    Node *temp = new Node();
    temp->data = x;
    temp->prev = NULL;
    temp->next = NULL;

    if(head == NULL){
        head  = temp;
        return;
    }

    head->prev=temp;
    temp->next = head;
    head = temp;
}

void InsertAtTailEnd(int x){
    Node *temp = new Node();
    temp->data = x;
    temp->prev = NULL;
    temp->next = NULL;

    if(head==NULL){
        head = temp;
        return;
    }

    Node *current = head;
    while(current->next!=NULL){
        current = current->next;
    }
    current->next = temp;
    temp->prev = current;
}

void Print(){
    Node *temp = head;
    cout << "The list is: ";
    while(temp!=NULL){
        cout << temp->data << " ";
        temp = temp->next;
    }
    cout << "\n";
}

int main(){
    head = NULL;
    InsertAtHead(200);
    InsertAtHead(99);
    InsertAtHead(130);
    InsertAtHead(190);
    InsertAtHead(210);
    Print();
    cout << "Inserting value 999 at end.\n";
    InsertAtTailEnd(999);
    Print();

    return 0;
}