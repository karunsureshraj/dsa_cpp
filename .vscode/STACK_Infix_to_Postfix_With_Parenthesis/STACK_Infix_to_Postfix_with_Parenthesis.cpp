#include "stdio.h"
#include "iostream"
#include "stack"
using namespace std;

stack<char> OperatorStack;
string Infix="((A+B)*C-D)*E";
string Postfix;

bool IsOperand(char token){
    if(token >='0' && token<= '9'){
        return true;
    } else if (token >='A' && token<= 'Z'){
        return true;
    }
    return false;
}

bool IsOperator(char token){
    if(token == '+' || token == '-' || token == '/' || token == '*' || token == '^')
        return true;
    return false;
}

int IdentifyWeight(char c){
    int weight = -1;
    switch (c)
    {
    case '+':
    case '-':
        weight = 1;
        break;
    case '*':
    case '/':
        weight = 2;
        break;
    case '^':
        weight = 3;
        break;
    }
    return weight;
}

bool IsTopOfStackOfHigherOrder(char top, char token){
    int w1 = IdentifyWeight(top);
    int w2 = IdentifyWeight(token);

    if(w1==w2){
        if(token == '^'){
            return false;
        }else{
            return true;
        }
    }
    return w1>w2 ? true: false;
}

bool IsClosingParenthesis(char c){
    return c == ')' ? true: false;
}

bool FoundOpeningParenthesis(char c){
    return c == '(' ? true: false;
}

void PrintStack(){
    stack<char> temp = OperatorStack;
    cout << "Stack count: " << OperatorStack.size() <<"\n";
    cout << "Stack elements: ";
    while(!temp.empty()){
        cout << temp.top() << " ";
        temp.pop();
    }
    cout << "\n";
}

void InfixtoPostfix(string Infix){
    for (int i = 0; i < Infix.length(); i++)
    {
        if(IsOperand(Infix[i])){
            cout << "Operand found. Adding to Result.\n";
            Postfix += Infix[i];
        }else if(IsClosingParenthesis(Infix[i])){
            cout << "Found closing Parenthesis. Popping out: ";
            while(!OperatorStack.empty() && !FoundOpeningParenthesis(OperatorStack.top())){
                cout << OperatorStack.top() << " ";
                Postfix += OperatorStack.top();
                OperatorStack.pop();
            }
            cout << OperatorStack.top() << " ";
            OperatorStack.pop();
            cout << "\n";
            cout << "Output String is: " << Postfix << "\n";
            PrintStack();
        }else if(FoundOpeningParenthesis(Infix[i])){
            cout << "Opening Parenthesis. Pushing to Stack.\n";
            OperatorStack.push(Infix[i]);
            PrintStack();
        }else if(IsOperator(Infix[i])){
            cout << "Operator found. Checking if top of stack has higher order.\n";
            cout << "Popping out: ";
            while(!OperatorStack.empty() && 
                  IsTopOfStackOfHigherOrder(OperatorStack.top(), Infix[i]) && 
                  !FoundOpeningParenthesis(OperatorStack.top())){
                cout << OperatorStack.top() << " ";
                Postfix += OperatorStack.top();
                OperatorStack.pop();
            }
            cout << "\n";
            OperatorStack.push(Infix[i]);
            PrintStack();
        }  
    }
    while(!OperatorStack.empty()){
        Postfix += OperatorStack.top();
        OperatorStack.pop();
    }
    cout << "The Postfix Expression is: " << Postfix << "\n";
}

int main(){
    InfixtoPostfix(Infix);
    return 0;
}