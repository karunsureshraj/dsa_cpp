#include "stdio.h"
#include "iostream"
using namespace std;

struct Node{
    int data;
    Node *next;
};
Node *head;

void Insert(int p, int v){
    Node* temp = new Node();
    temp->data = v;
    
    if(p==1){
        temp->next = head;
        head = temp;
        return;
    }

    Node* temp1 = head;
    for (int i = 0; i < p-2; i++)
    {
        temp1 = temp1->next;
    }
    
    temp->next = temp1->next;
    temp1->next = temp;
    
}

void Print(){
    Node* temp = head;
    cout << "The list is: ";
    while(temp!=NULL){
        cout<< temp->data << " ";
        temp = temp->next;
    }
    cout<< "\n";
}

void PrintUsingRecursion(Node* link){
    if(link==NULL){
        cout << "\n";
        return;
    }
    cout << link->data << " ";
    PrintUsingRecursion(link->next);
}


void ReversePrintUsingRecursion(Node* link){
    if(link==NULL){
        return;
    }
    ReversePrintUsingRecursion(link->next);
    cout << link->data << " ";
}

int main(){
    head = NULL;
    Insert(1,10);
    Insert(2,99);
    Insert(3,400);
    Insert(4,90);
    //Print();
    PrintUsingRecursion(head);
    cout << "Inserting at position 2\n";
    Insert(2,345);
    Print();
    cout << "Reversing the List\n";
    ReversePrintUsingRecursion(head);
    return 0;

}