#include "stdio.h"
#include "iostream"
using namespace std;

struct Node
{
    int data;
    Node* next;
};

Node* head;

void Insert(int x){
    Node* temp = new Node();
    temp->data=x;
    
    /*
     temp->next=NULL;

     if(head!=NULL)
         temp->next = head;
    */

    /*
     This is possible because in main() head is set to NULL.
     So if LinkedList is empty, head will be NULL, thus setting temp->next to NULL
     If not, the non-NULL value of head will be copied over to head
    */
    temp->next = head; 
    head = temp;
}

void Print(){
    Node* temp = head;
    while (temp !=NULL){
        cout <<  " " << temp->data;
        temp = temp->next;
    }
    printf("\n");
}

int main()
{
    head = NULL;
    int count,n;
    cout <<"How many numbers? \n";
    cin >> count;

    for (int i = 0; i < count; i++)
    {
        cout << "Enter the number: \n";
        cin >> n;

         Insert(n);
         Print();
     }
    return 0;
}