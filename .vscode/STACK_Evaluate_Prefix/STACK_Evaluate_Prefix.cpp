#include "stdio.h"
#include "iostream"
#include "stack"
using namespace std;

stack<int> OperandStack;
string Expr = "-+*23+549";

bool IsOperator(char v){
    if(v == '+'){
        return true;
    }else if(v == '-'){
        return true;
    }else if(v == '*'){
        return true;
    }else if(v == '/'){
        return true;
    }
    return false;
}

bool IsOperand(char v){
    if(v>='0' && v<='9'){
        return true;
    }
    return false;
}

int Calculate(int Op1, int Op2, char Operator){
    if(Operator == '+'){
        return Op1 + Op2;
    }else if(Operator == '-'){
        return Op1 - Op2;
    }else if(Operator == '*'){
        return Op1 * Op2;
    }else if(Operator == '/'){
        return Op1 / Op2;
    }else
    return 0;    
}
void ParseAndStore(string E){
    for (int i = E.length(); i > 0; i--)
    {
        if(IsOperand(E[i])){
            int Operand = (int)E[i]-48;
            cout << "Pushing value: " << Operand << " to Stack.\n";
            OperandStack.push(Operand);
        }else if(IsOperator(E[i])){
            int Op1 = OperandStack.top();
            OperandStack.pop();
            int Op2 = OperandStack.top();
            OperandStack.pop();
            cout << "Found an Operator, Popping values: " << Op1 << " " << Op2 << " from Stack.\n";
            int result = Calculate(Op1, Op2, E[i]);
            cout << "The result is: " << result << ". Pushing to Stack.\n";
            OperandStack.push(result);
        }
    } 
    cout << "Final Result: " << OperandStack.top() << "\n";   
}
int main(){
    ParseAndStore(Expr);    
    return 0;
}
