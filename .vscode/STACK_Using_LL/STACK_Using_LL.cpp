#include "stdio.h"
#include "iostream"
using namespace std;

struct Node
{
    int data;
    Node *next;
};

Node *head;

void Push(int x){
    Node *temp = new Node();
    temp->data = x;

    temp->next = head;
    head = temp;
}

void Pop(){
    if(head!=NULL){
        cout << "Popping out " << head->data << " from the list.\n";
        Node * temp = head;
        head = head->next;
        delete temp;
        return;
    }
    cout << "Cannot POP from an empty list.\n";
}

void Print(){
    Node *temp = head;
    cout << "The elements in list are: ";
    while(temp!=NULL){
        cout << temp->data << " ";
        temp = temp->next;
    }
    cout << "\n";
}

int main(){
    head = NULL;
    
    Push(10);
    Push(30);
    Push(5);
    Push(12);
    Print();
    Pop();
    Print();
    Pop();
    Print();
    return 0;
}