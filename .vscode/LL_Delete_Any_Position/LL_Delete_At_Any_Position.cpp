#include "stdio.h"
#include "iostream"
using namespace std;

struct Node
{
    int data;
    Node* next;
};
Node* head;

void Insert(int p, int v){
    Node* temp = new Node();
    temp->data = v;
    
    if(p==1){ 
        temp->next = head;
        head = temp;
        return;
    }

    Node* temp1 = head;
    for (int i = 0; i < p-2; i++)
    {
        temp1 = temp1->next;
    }
    
    temp->next = temp1->next;
    temp1->next = temp;
}
void Delete(int p){
    if(p==1){
        if(head!=NULL){
            Node* temp = head;
            head = temp->next;
            delete temp;
            cout << "Deleted node at Position: " << p <<".\n";
        }
        cout<< "You cannot delete an Empty List\n";
        return;
    }

    Node* temp1 = head;
    for (int i = 0; i < p-2; i++)
    {
        temp1 = temp1->next;
    }
    Node* temp2 = temp1->next;

    temp1->next = temp2->next;
    delete temp2;
    cout << "Deleted node at Position: " << p <<".\n";
}

void Print(){
    Node* temp = head;

    cout<< "The list is: ";
    while(temp!=NULL){
        cout << temp->data << " ";
        temp = temp->next;
    }
    cout<<"\n";

}

int main(){
    head = NULL;
    int p;
    Insert(1,10);
    Insert(2,100);
    Insert(3,250);
    Print();
    
    cout<<"Inserting 400 at Position 2\n";
    Insert(2,400);
    Print();
    
    cout<< "Enter the position of node to delete from list\n";
    cin >> p;
    Delete(p);
    Print();

    return 0;
}